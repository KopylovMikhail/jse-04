package ru.kopylov.tm.view;

import ru.kopylov.tm.service.TaskService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class TaskView {

    private TaskService taskService;

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public TaskView(TaskService taskService) {
        this.taskService = taskService;
    }

    public void create() {
        System.out.println("[TASK CREATE]\n" +
                "ENTER NAME:");
        try {
            String taskName = reader.readLine();
            if (taskService.create(taskName)) System.out.println("[OK]\n");
            else System.out.println("[SUCH A TASK EXISTS.]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void list() {
        List<String> taskList = taskService.list();

        System.out.println("[TASK LIST]");
        int i = 1;
        for (String task : taskList) {
            System.out.println(i++ + ". " + task);
        }
        System.out.print("\n");
    }

    public void update() {
        String nameOld, nameNew;

        System.out.println("[TASK UPDATE]\n" +
                "ENTER EXISTING TASK NAME:");
        try {
            nameOld = reader.readLine();
            System.out.println("ENTER NEW TASK NAME:");
            nameNew = reader.readLine();
            if (taskService.update(nameOld, nameNew))
                System.out.println("[TASK " + nameOld + " UPDATED TO " + nameNew + "]\n");
            else System.out.println("SUCH A TASK DOES NOT EXIST.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void remove() {
        System.out.println("[TASK REMOVE]\n" +
                "ENTER EXISTING TASK NAME:");
        try {
            String taskName = reader.readLine();
            if (taskService.remove(taskName))
                System.out.println("[TASK " + taskName + " REMOVED]\n");
            else System.out.println("SUCH A TASK DOES NOT EXIST.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clear() {
        taskService.clear();
        System.out.println("[ALL TASKS REMOVED]\n");
    }

    public void help() {
        System.out.println("task-create: Create new task.\n" +
                "task-list: Show all tasks.\n" +
                "task-update: Update selected task.\n" +
                "task-remove: Remove selected task.\n" +
                "task-clear: Remove all tasks.");
    }

}
