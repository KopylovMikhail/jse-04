package ru.kopylov.tm;

/**
 * @author Mikhail Kopylov
 * task/project manager
 */

public class Application {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
