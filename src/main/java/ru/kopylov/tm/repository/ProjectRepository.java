package ru.kopylov.tm.repository;

import ru.kopylov.tm.entity.Project;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ProjectRepository {

    public static Map<String, Project> projectMap = new LinkedHashMap<>();

    public void merge(Project project) {
        projectMap.put(project.getId(), project);
    }

    public Project persist(Project project) {
        return projectMap.putIfAbsent(project.getId(), project);
    }

    public List<Project> findAll() {
        List<Project> projects = new ArrayList<Project>(projectMap.values());
        return projects;
    }

    public boolean remove(String projectName) {
        return projectMap.entrySet()
                .removeIf(entry -> entry.getValue().getName().equals(projectName));
    }

    public void removeAll() {
        projectMap.clear();
    }

    public Project findOne(String projectName) {
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (projectName.equals(entry.getValue().getName())) {
                return entry.getValue();
            }
        }
        return null;
    }

}
