package ru.kopylov.tm.service;

import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public boolean create(String taskName) {
        Task task = new Task();

        task.setName(taskName);
        return !task.equals(taskRepository.persist(task));
    }

    public List<String> list() {
        List<String> taskNameList = new ArrayList<>();
        for (Task task : taskRepository.findAll()) {
            taskNameList.add(task.getName());
        }
        return taskNameList;
    }

    public boolean update(String nameOld, String nameNew) {
        try {
            Task task = taskRepository.findOne(nameOld);
            task.setName(nameNew);
            taskRepository.merge(task);
            return true;
        } catch (NullPointerException e) {
            return false;
        }
    }

    public boolean remove(String taskName) {
        return taskRepository.remove(taskName);
    }

    public void clear() {
        taskRepository.removeAll();
    }

}
