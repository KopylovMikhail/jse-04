package ru.kopylov.tm.view;

import ru.kopylov.tm.service.ProjectService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class ProjectView {

    private ProjectService projectService;

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public ProjectView(ProjectService projectService) {
        this.projectService = projectService;
    }

    public void create() {
        System.out.println("[PROJECT CREATE]\n" +
                "ENTER NAME:");
        try {
            String projectName = reader.readLine();
            if (projectService.create(projectName)) System.out.println("[OK]\n");
            else System.out.println("[SUCH A PROJECT EXISTS.]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void list() {
        List<String> projectList = projectService.list();

        System.out.println("[PROJECT LIST]");
        int i = 1;
        for (String project : projectList) {
            System.out.println(i++ + ". " + project);
        }
        System.out.print("\n");
    }

    public void update(){
        String nameOld, nameNew;

        System.out.println("[PROJECT UPDATE]\n" +
                "ENTER EXISTING PROJECT NAME:");
        try {
            nameOld = reader.readLine();
            System.out.println("ENTER NEW PROJECT NAME:");
            nameNew = reader.readLine();
            if (projectService.update(nameOld, nameNew))
                System.out.println("[PROJECT " + nameOld + " UPDATED TO " + nameNew + "]\n");
            else System.out.println("SUCH A PROJECT DOES NOT EXIST.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void remove() {
        System.out.println("[PROJECT REMOVE]\n" +
                "ENTER EXISTING PROJECT NAME:");
        try {
            String projectName = reader.readLine();
            if (projectService.remove(projectName))
                System.out.println("[PROJECT " + projectName + " REMOVED]\n");
            else System.out.println("SUCH A PROJECT DOES NOT EXIST.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clear() {
        projectService.clear();
        System.out.println("[ALL PROJECTS REMOVED]\n");
    }

    public void setTask() {
        System.out.println("ENTER EXISTING PROJECT NAME:");
        try {
            String projectName = reader.readLine();
            System.out.println("ENTER EXISTING TASK NAME:");
            String taskName = reader.readLine();
            projectService.setTask(projectName, taskName);
            System.out.println("[OK]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void tasksList() {
        System.out.println("ENTER EXISTING PROJECT NAME:");
        try {
            String projectName = reader.readLine();
            List<String> taskList = projectService.tasksList(projectName);
            System.out.println("TASKS LIST FOR PROJECT " + projectName + ":");
            int n = 1;
            for (String taskName : taskList) {
                System.out.println(n++ + ". " + taskName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.print("\n");
    }

    public void help() {
        System.out.println("project-create: Create new project.\n" +
                "project-list: Show all projects.\n" +
                "project-update: Update selected project.\n" +
                "project-remove: Remove selected project.\n" +
                "project-clear: Remove all projects.\n" +
                "project-set-task: Assign a task to a project.\n" +
                "project-tasks-list: Project task list.\n");
    }

}
