package ru.kopylov.tm;

import ru.kopylov.tm.enumerated.Command;
import ru.kopylov.tm.repository.ProjectRepository;
import ru.kopylov.tm.repository.TaskOwnerRepository;
import ru.kopylov.tm.repository.TaskRepository;
import ru.kopylov.tm.service.ProjectService;
import ru.kopylov.tm.service.TaskService;
import ru.kopylov.tm.view.ProjectView;
import ru.kopylov.tm.view.TaskView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;

public class Bootstrap {

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private ProjectRepository projectRepository = new ProjectRepository();

    private TaskRepository taskRepository = new TaskRepository();

    private TaskOwnerRepository taskOwnerRepository = new TaskOwnerRepository();

    private ProjectService projectService = new ProjectService(projectRepository, taskRepository, taskOwnerRepository);

    private TaskService taskService = new TaskService(taskRepository);

    private ProjectView projectView = new ProjectView(projectService);

    private TaskView taskView = new TaskView(taskService);

    public void init() {
        Command command = Command.UNKNOWN_COMMAND;
        String inputString;

        System.out.println("*** WELCOME TO TASK MANAGER ***");
        do {
            try {
                inputString = reader.readLine()
                        .toUpperCase(Locale.ENGLISH)
                        .replace('-', '_');
                command = Command.valueOf(inputString);
                readCommand(command);
            } catch (IllegalArgumentException e) {
                command = Command.UNKNOWN_COMMAND;
                System.out.println("Command not found.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (command != Command.EXIT);
    }

    private void readCommand(Command command) {
        switch (command) {
            case PROJECT_CREATE:
                projectView.create();
                break;
            case PROJECT_LIST:
                projectView.list();
                break;
            case PROJECT_UPDATE:
                projectView.update();
                break;
            case PROJECT_REMOVE:
                projectView.remove();
                break;
            case PROJECT_CLEAR:
                projectView.clear();
                break;
            case PROJECT_SET_TASK:
                projectView.setTask();
                break;
            case PROJECT_TASKS_LIST:
                projectView.tasksList();
                break;
            case TASK_CREATE:
                taskView.create();
                break;
            case TASK_LIST:
                taskView.list();
                break;
            case TASK_UPDATE:
                taskView.update();
                break;
            case TASK_REMOVE:
                taskView.remove();
                break;
            case TASK_CLEAR:
                taskView.clear();
                break;
            case EXIT:
                break;
            case HELP:
                help();
                break;
            default:
                System.out.println("Command not found.");
        }
    }

    private void help() {
        System.out.println("help: Show all commands.");
        projectView.help();
        taskView.help();
        System.out.print("\n");
    }

}
